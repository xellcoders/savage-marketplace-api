import fs from "fs";
import https from "https";
import express from 'express';
import http from 'http';
import cors from 'cors';
import passport from "passport";
import AuthRouter from "./routes/auth.js";
import UserRouter from "./routes/user.js";
import NFTRouter from "./routes/nft.js";
import ItemRouter from "./routes/item.js";
import StatsRouter from "./routes/stats.js";
import TransactionRouter from "./routes/transaction.js";
import UtilRouter from "./routes/util.js";
import WalletRouter from "./routes/wallet.js";
import IndexRouter from "./routes/index.js";
import APIRouter from "./routes/api.js";
import Logger from "./util/logger.js";
import config from "./config.js";
import BearerPassport from "./passport/bearer.js";
import { initServices } from "./services/index.js";

const logger = new Logger();
const app = express();
const authRouter = new AuthRouter( logger.instance( config.logger.elastic.indexes.auth ) );
const userRouter = new UserRouter( logger.instance( config.logger.elastic.indexes.user ) );
const nftRouter = new NFTRouter( logger.instance( config.logger.elastic.indexes.nft ) );
const itemRouter = new ItemRouter( logger.instance( config.logger.elastic.indexes.item ) );
const statsRouter = new StatsRouter( logger.instance( config.logger.elastic.indexes.stats ) );
const walletRouter = new WalletRouter( logger.instance( config.logger.elastic.indexes.wallet ) );
const indexRouter = new IndexRouter( logger.instance( config.logger.elastic.indexes.index ) );
const apiRouter = new APIRouter( logger.instance( config.logger.elastic.indexes.api ) );
const transactionRouter = new TransactionRouter( logger.instance( config.logger.elastic.indexes.transaction ) );
const utilRouter = new UtilRouter( logger.instance( config.logger.elastic.indexes.util ) );

app.use( cors() );
app.use( express.json( { limit: '50mb' }) );
app.use( express.urlencoded( { extended: false, limit: '50mb' } ) );

passport.serializeUser( function( user, done ) {
	done( null, user );
} );
passport.deserializeUser( function( uid, done ) {
	done( null, uid );
} );

passport.use( 'bearer', new BearerPassport( config.passport.bearer ).getPassport() );

initServices().then( () => {
	console.log( 'Services initialized' );
} ).catch( error => {
	console.error( 'Initializing services', error );
	process.exit( 1 );
} );

app.use( function( req, res, next ) {
	res.setHeader( 'Strict-Transport-Security', 'max-age=15724800; includeSubDomains' );
	next();
} );

app.use( passport.initialize() );

app.use( '/auth', authRouter.getRouter() );
app.use( '/user', userRouter.getRouter() );
app.use( '/nft', nftRouter.getRouter() );
app.use( '/item', itemRouter.getRouter() );
app.use( '/stats', statsRouter.getRouter() );
app.use( '/tx', transactionRouter.getRouter() );
app.use( '/wallet', walletRouter.getRouter() );
app.use( '/util', utilRouter.getRouter() );
app.use( '/api', apiRouter.getRouter() );
app.use( '/', indexRouter.getRouter() );

try {
	if( !config.server.ssl.enabled ) {
		const server = http.createServer( app );

		server.listen( config.server.port, config.server.ip, function() {
			console.log( `${config.server.name} v${config.server.ver} HTTP | port`, config.server.port );
		} );
	} else {
		const privateKey = fs.readFileSync( config.server.ssl.key, 'utf8' );
		const certificate = fs.readFileSync( config.server.ssl.crt, 'utf8' );
		const credentials = { key: privateKey, cert: certificate };
		const ssl = https.createServer( credentials, app );

		ssl.listen( config.server.port, config.server.ip, function() {
			console.log( `${config.server.name} v${config.server.ver} HTTPS | port`, config.server.port );
		} );
	}
} catch( error ) {
	console.error( error );
	process.exit();
}