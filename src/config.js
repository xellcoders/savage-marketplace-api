export default {
	server: {
		name: process.env.NAME || 'Savage Marketplace API',
		ver: process.env.VERSION || '1.0',
		port: process.env.PORT || 8080,
		ip: process.env.BINDING_IP || '0.0.0.0',
		ssl: {
			enabled: process.env.SSL_ENABLED || false,
			key: process.env.SSL_KEY || '/certs/cert.key',
			crt: process.env.SSL_CRT || '/certs/cert.crt'
		},
		domain: process.env.HOST_DOMAIN || 'http://localhost:8080',
		front: process.env.FONT_DOMAIN || 'http://localhost:3000'
	},
	sendgrid: {
		apikey: process.env.SENDGRID_APIKEY || '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',
		sender: process.env.SENDGRID_SENDER || 'psychostrauss@gmail.com',
		subject: process.env.SENDGRID_SENDER || 'Savage Marketplace',
		activationTemplate: process.env.SENDGRID_ACTIVATION_TEMPLATE || 'd-74376eb941b3458d9c6f087030929f60',
		developerTemplate: process.env.SENDGRID_DEVELOPER_TEMPLATE || 'd-13dfb6e6a18d4ee98c58da13d146df24'
	},
	google: {
		clientId: process.env.GOOGLE_API_KEY || '460585283800-qpfpejgpik4eop5dfsdgc0g746fjr38t.apps.googleusercontent.com',
		clientSecret: process.env.GOOGLE_API_KEY || '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'
	},
	networks: {
		ethereum: {
			rpc: "https://goerli.infura.io/v3/5adb88ee4ab34a67951eacf3a87a4922",
			tokens: {
				nsc: '0xab8f419b6108574f0fa9a03037be0db37aad9a14'
			},
			account: {
				address: process.env.ETH_ACCOUNT || '0x758ebb6d75dcfd33abdabcf8de02d9477e341bb7',
				privateKey: process.env.ETH_PRIVATE_KEY || 'a380ad6aa35a84a93262cede5574f187576eccb025651feba3f6ac2177233437'
			}
		}
	},
	storage: {
		baseUri: 'https://savage-api.xellcoders.com/nft/',
		keyFile: process.env.STORAGE_KEY_FILE || 'gcloudKeyFile.json',
		bucket: process.env.STORAGE_BUCKET || 'savage-marketplace'
	},
	mongo: {
		url: process.env.MONGODB_URL || 'mongodb://localhost:27017/savage-marketplace'
	},
	passport: {
		bearer: {
			accessMethodID: 'bearer',
			passReqToCallback: true
		}
	},
	logger: {
		level: process.env.LOGGER_LEVEL || 'error',
		elastic: {
			enabled: process.env.ELASTIC_ENABLED || false,
			node: process.env.ELASTIC_NODE_URL,
			username: process.env.ELASTIC_USERNAME,
			password: process.env.ELASTIC_PASSWORD,
			indexes: {
				auth: process.env.ELASTIC_AUTH_INDEX || 'auth',
				user: process.env.ELASTIC_USER_INDEX || 'user',
				nft: process.env.ELASTIC_NFT_INDEX || 'nft',
				item: process.env.ELASTIC_ITEM_INDEX || 'item',
				stats: process.env.ELASTIC_STATS_INDEX || 'stats',
				wallet: process.env.ELASTIC_WALLET_INDEX || 'wallet',
				index: process.env.ELASTIC_INDEX_INDEX || 'index',
				api: process.env.ELASTIC_API_INDEX || 'api',
				transaction: process.env.ELASTIC_TX_INDEX || 'tx',
				util: process.env.ELASTIC_UTIL_INDEX || 'util',
			}
		}
	},
	redis: {
		url: process.env.REDIS_URL || 'redis://localhost:6379'
	}
}