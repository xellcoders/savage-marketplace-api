import Passport from "./passport.js";
import Bearer from "passport-http-bearer";
import { redisService } from "../services/index.js";

export default class BearerPassport extends Passport {

	constructor( config ) {
		super( config, Bearer.Strategy );
	}

	async callback( req, token, done ) {
		try {
			if( !token ) return done( 'Missing authentication token' );
			const user = await redisService.getSession( token );
			if( !user ) return done( 'Invalid authentication token' );

			return done( null, {
				token,
				id: user.id,
				role: user.role
			} );
		} catch( error ) {
			return done( 'Invalid authentication token' );
		}
	}
}