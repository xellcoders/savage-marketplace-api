import passport from "passport";

export default class Passport {
	constructor( config, Strategy ) {
		this.passport = new Strategy( config, ( ...params ) => this.callback.apply( this, params ) );
	}

	static bearerAuthentication( callback ) {
		return passport.authenticate( 'bearer', { session: false }, callback );
	}

	getPassport() {
		return this.passport;
	}

	callback( ...params ) {
	}
}