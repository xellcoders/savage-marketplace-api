export function isImage( mimetype ) {
  return mimetype.startsWith( 'image' );
}

export function isAudio( mimetype ) {
  return mimetype.startsWith( 'audio' );
}

export function isVideo( mimetype ) {
  return mimetype.startsWith( 'video' );
}

export function isModel( mimetype ) {
  return mimetype.startsWith( 'model' );
}