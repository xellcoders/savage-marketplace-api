import ethers from "ethers";
import elliptic from "elliptic";
import ethutils from "ethereumjs-util";
import axios from "axios";

const secp256k1 = new elliptic.ec( 'secp256k1' );

export function createEthereumKeyPair() {
	const kp = secp256k1.genKeyPair();
	const publicKey = kp.getPublic( 'hex' );
	const privateKey = kp.getPrivate( 'hex' );
	const address = toEthereumAddress( publicKey );
	return { address, privateKey }
}

export function toEthereumAddress( hexPublicKey ) {
	return `0x${ethutils.keccak( Buffer.from( hexPublicKey.slice( 2 ), 'hex' ) )
		.slice( -20 )
		.toString( 'hex' )}`
}

export async function getBalance( address, network ) {
	const provider = ethers.getDefaultProvider( network, {
		etherscan: 'M2YM7BXNGUCAXFVV2CXR227RHJVGFMU1AQ'
	});
	const wei = await provider.getBalance( address );
	return ethers.utils.formatEther( wei );
}

export async function getPrice(){
	return axios.get( 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/quotes/latest?symbol=ETH', {
		headers: {
			'X-CMC_PRO_API_KEY': '7a9f8197-5a21-4f0d-aaf2-09cdcc4203b1',
		}
	} ).then( result => result.data).then( result => ({
		price: result.data.ETH.quote.USD.price,
		change: result.data.ETH.quote.USD.percent_change_24h
	}) ).catch( () => 0 );
}

export async function hasBalance( value, currency, balance ){
	if( currency.toLowerCase() === 'eth' ) return balance >= value;
	const price = await getPrice();
	return balance >= value * price.price * 20;
}

export async function toEth( value, currency ){
	if( currency.toLowerCase() === 'eth' ) return value;
	const price = await getPrice();
	return value / (price.price * 20);
}