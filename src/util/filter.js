export default function( items, filters ) {
  if( !filters ) return items;
  const { rarity, minPrice, maxPrice, game, creator, collection, tags, rating, search } = JSON.parse( Buffer.from( filters, 'base64' ).toString() );
  const found = search && search !== '' ? items.filter( i => i.nft.name.toLowerCase().includes( search.toLowerCase() ) ) : items;
  return found.filter( item => !rarity || rarity?.includes( item.nft.rarity ) )
      .filter( item => ( item.price || item.bid || item.start || 0 ) >= (isNaN( minPrice ) ? 0 : minPrice) )
      .filter( item => ( item.price || item.bid || item.start || 0 ) <= (isNaN( minPrice ) ? Number.MAX_VALUE : maxPrice) )
      .filter( item => creator === 'all' || item.nft.creator?.username?.toLowerCase().startsWith( creator.toLowerCase() ) )
      .filter( item => collection === 'all' || item.nft.collection?.name === collection )
      .filter( item => tags.length === 0 || item.nft.tags?.filter(value => tags.map( tag => tag.replace('#', '') ).includes( value.replace('#', '') )).length > 0 )
      .filter( item => rating === 'all' || item.rating === rating );
}