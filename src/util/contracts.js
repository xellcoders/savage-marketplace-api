import ethers from "ethers";
import NFT from "./nft.json";
import config from "../config.js";
import * as CONSTANTS from "constants";

const ERC20_ABI = [
  "function balanceOf(address owner) view returns (uint256)",
  "function decimals() view returns (uint8)",
  "function totalSupply() view returns (uint256)",
  "function symbol() view returns (string)",
  "function name() view returns (string)",
]

export async function deployNFT( name, symbol, uri, network = 'ethereum' ){
  const { rpc, account } = config.networks['ethereum'];
  const NFTContract = new ethers.ContractFactory(
      NFT.abi, NFT.bytecode,
      new ethers.Wallet( '0xa380ad6aa35a84a93262cede5574f187576eccb025651feba3f6ac2177233437', new ethers.providers.JsonRpcProvider( rpc ) )
  );
  const nft = await NFTContract.deploy( name, symbol, uri, { gasLimit: 11500000 } );
  return { address: nft.address, hash: nft.deployTransaction.hash };
}

export async function mintItem( tokenAddress, userAddress, tokenId, uri, network = 'ethereum' ){
  const { rpc, account } = config.networks['ethereum'];
  const nft = new ethers.Contract( tokenAddress, NFT.abi,
      new ethers.Wallet( '0xa380ad6aa35a84a93262cede5574f187576eccb025651feba3f6ac2177233437', new ethers.providers.JsonRpcProvider( rpc ) )
  );
  const tx = await nft.mintWithTokenURI( userAddress, tokenId, uri, { gasLimit: 11500000 } );
  return { hash: tx.hash };
}

export async function getERC20Balance( account, token, network = 'ethereum' ){
  const { rpc } = config.networks['ethereum'];
  const erc20 = new ethers.Contract( token, ERC20_ABI, new ethers.providers.JsonRpcProvider( rpc ) );
  return (await erc20.balanceOf( account )).toString() / 1e8;
}