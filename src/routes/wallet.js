import Router from "./router.js";
import { userService, walletService } from "../services/index.js";
import config from "../config.js";

export default class WalletRouter extends Router {

  constructor( logger ) {
    super( logger );
  }

  init() {
    this.get( '/', ['AUTHENTICATED'], this.register );
    this.get( '/balance/:address', ['AUTHENTICATED'], this.balance );
  }

  async register( req ) {
    const { user: { id } } = req;
    const user = await userService.getUserById( id );
    const wallets = walletService.getUserWallets( user );
    return wallets.map( wallet => ( {
      id: wallet._id,
      chain: wallet.chain,
      address: wallet.address,
      manager: wallet.manager
    } ) );
  }

  async balance( req ) {
    const { address } = req.params;
    const { network, currency } = req.query;
    if( !config.networks[network] || !address.startsWith('0x') ) return 0;
    switch( currency ) {
      case 'eth':
        return await walletService.getEthereumBalance( address, network );
      case 'nsc':
        return await walletService.getTokenBalance( address, config.tokens.nsc, network );
    }
    return 0;
  }

}