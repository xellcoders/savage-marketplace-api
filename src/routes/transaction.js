import Router from "./router.js";
import { transactionService } from "../services/index.js";

export default class TransactionRouter extends Router {

	constructor( logger ) {
		super( logger );
	}

	init() {
		this.get( '/', ['AUTHENTICATED'], this.userTransactions );
	}

	async userTransactions( req ) {
		const { user } = req;
		const transactions = await transactionService.getUserTransactions( user.id );
		return transactions.map( tx => tx._doc );
	}

}