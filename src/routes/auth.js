import Router from "./router.js";
import { authService, redisService, sendgridService, userService, walletService } from "../services/index.js";
import { INVALID_CREDENTIALS, USER_NOT_FOUND } from "../services/auth.js";
import APIError from '../util/error.js';
import * as google from "google-auth-library";
import config from "../config.js";

export default class AuthRouter extends Router {

  constructor( logger ) {
    super( logger );
  }

  init() {
    this.post( '/login', ['PUBLIC'], this.login );
    this.get( '/current', ['AUTHENTICATED'], this.current );
    this.post( '/google', ['PUBLIC'], this.google );
  }

  async login( req ) {
    const { email, password } = req.body;
    const result = await authService.login( email, password );
    if( result === USER_NOT_FOUND ) throw new APIError( 'User not found', result, 404 );
    if( result === INVALID_CREDENTIALS ) throw new APIError( 'Invalid user credentials', result, 500 );
    const token = await redisService.setSession( result );
    return { token }
  }

  async current( req ) {
    const user = await userService.getUserById( req.user.id );
    const wallets = await walletService.getUserWallets( user );
    return {
      ...user._doc,
      password: undefined,
      wallet: wallets.length > 0 ? wallets[0].address : ''
    }
  }

  async google( req ) {
    const client = new google.default.OAuth2Client( config.google.clientSecret )
    const { idToken } = req.body
    const ticket = await client.verifyIdToken( {
      idToken,
      audience: config.google.clientId
    } );

    const { name, email } = ticket.getPayload();

    const current = await userService.getUserByEmail( email );
    if( current ){
      const token = await redisService.setSession( current._doc );

      return {
        ...current._doc,
        token,
        password: undefined
      }
    }

    const user = await userService.register( name, email, '', 'consumer' );
    walletService.create( user ).catch( error => {
      this.logger.error( `createWallet`, {
        response: {
          code: error.code,
          message: error.message,
          stack: error.stack

        },
        request: JSON.stringify( req.body )
      } )
    } );

    const code = await redisService.setActivation( user );

    sendgridService.sendActivation( email, code ).catch( error => {
      this.logger.error( `register sendActivation`, {
        response: {
          code: error.code,
          message: error.message,
          stack: error.stack
        },
        request: JSON.stringify( req.body )
      } )
    } );

    const token = await redisService.setSession( user._doc );

    return {
      ...user._doc,
      token,
      password: undefined
    }

  }
}