import express from 'express';
import apicache from "apicache";
import multer from "multer";
import Passport from "../passport/passport.js";
import swaggerUi from "swagger-ui-express";

export default class Router {

	constructor( logger ) {
		apicache.options( {
			appendKey: ( req, res ) => `${req.path}:${JSON.stringify( req.body )}:${JSON.stringify( req.query )}:${JSON.stringify( req.params )}`
		} )
		this.logger = logger;
		this.router = express.Router();
		this.cache = apicache.middleware;
		this.init();
	}

	init() {
	}

	swagger( path, document ) {
		this.router.use( path, swaggerUi.serve );
		this.router.get( path, swaggerUi.setup( document ) );
	}

	cached( path, policies, cache, callback ) {
		this.router.get( path, this._bindCustomResponses, this._checkAuthorization( policies ), this.cache( cache ), this._getCallback( callback ) );
	}

	get( path, policies, callback ) {
		this.router.get( path, this._bindCustomResponses, this._checkAuthorization( policies ), this._getCallback( callback ) );
	}

	put( path, policies, callback ) {
		this.router.put( path, this._bindCustomResponses, this._checkAuthorization( policies ), this._getCallback( callback ) );
	}

	post( path, policies, callback ) {
		this.router.post( path, this._bindCustomResponses, this._checkAuthorization( policies ), this._getCallback( callback ) );
	}

	delete( path, policies, callback ) {
		this.router.delete( path, this._bindCustomResponses, this._checkAuthorization( policies ), this._getCallback( callback ) );
	}

	multer( path, policies, storage, callback ) {
		const uploader = multer({ storage });
		this.router.post( path, this._bindCustomResponses, this._checkAuthorization( policies ), uploader.any(), this._getCallback( callback ) );
	}

	getRouter() {
		return this.router;
	}

	_checkAuthorization( policies ) {
		return ( req, res, next ) => {
			return Passport.bearerAuthentication( ( error, user ) => {
				if( error && policies[0] !== 'PUBLIC' ) return res.sendError( { code: 0, message: error } );
				if( !user && policies[0] !== 'PUBLIC' ) return res.sendError( { code: 1, message: 'No token provided' } );
				req.user = user;
				if( policies[0] === 'AUTHENTICATED' || policies[0] === 'PUBLIC' ) return next();
				if( !policies.includes( user.role.toUpperCase() ) ) return res.sendError( {
					code: 2,
					message: 'Not authorized'
				} );
				next();
			} )( req, res, next );
		}
	}

	_getCallback( callback ) {
		return async( ...params ) => {
			try {
				const startTime = new Date().getTime();
				const response = await callback.apply( this, params )
				const endTime = new Date().getTime();
				const latency = endTime - startTime;
				params[1].sendSuccess( response );
				this.logger.debug( `${params[0].method} ${params[0].path}`, {
					response: {
						status: 200,
						executionTime: latency
					},
					request: JSON.stringify( params[0].body )
				} );
			} catch( error ) {
				this.logger.error( `${params[0].method} ${params[0].path}`, {
					response: {
						code: error.code,
						message: error.message,
						stack: error.stack
					},
					request: JSON.stringify( params[0].body )
				} );
				params[1].sendError( { code: error.code, message: error.message }, error.http );
			}
		}
	}

	_bindCustomResponses( req, res, next ) {
		res.sendSuccess = ( payload, executionTime ) => {
			res.status( 200 ).json( payload );
		};
		res.sendError = ( error, status = 500 ) => {
			res.status( status ).json( error );
		};
		next();
	}
}