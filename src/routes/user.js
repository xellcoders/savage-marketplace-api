import Router from "./router.js";
import {
	itemService,
	redisService,
	sendgridService, statsService,
	userService,
	walletService
} from "../services/index.js";
import config from "../config.js";
import APIError from "../util/error.js";

export default class UserRouter extends Router {

	constructor( logger ) {
		super( logger );
	}

	init() {
		this.get( '/', ['ADMIN'], this.list );
		this.post( '/', ['PUBLIC'], this.register );
		this.put( '/developer', ['AUTHENTICATED'], this.developer );
		this.put( '/username', ['AUTHENTICATED'], this.username );
		this.put( '/password', ['AUTHENTICATED'], this.password );
		this.put( '/interest', ['AUTHENTICATED'], this.interest );
		this.put( '/currency', ['AUTHENTICATED'], this.currency );
		this.put( '/role/:id', ['ADMIN'], this.role );
		this.put( '/status/:id', ['ADMIN'], this.status );
		this.get( '/activate/:code', ['PUBLIC'], this.activate );
		this.get( '/topcreators', ['AUTHENTICATED'], this.topcreators );
		this.get( '/topusers', ['AUTHENTICATED'], this.topusers );
		this.get( '/:id', ['AUTHENTICATED'], this.user );
	}

	async user( { params: { id } } ){
		const user = await userService.getUserById( id );
		const wallets = await walletService.getUserWallets( user );
		return {
			username: user.username,
			avatar: user.avatar,
			wallet: wallets[0].address
		};
	}

	async list( { query: { filters } } ){
		const { username, role, status } = JSON.parse( Buffer.from( filters, 'base64' ).toString() );
		const users = await userService.getUsers( username, role?.split(','), status?.split(',') );
		return await Promise.all( users.map( async user => ({
			id: user._id,
			avatar: user.avatar,
			username: user.username,
			email: user.email,
			status: user.status,
			role: user.role,
			...(await statsService.getUserTransactionStats( user ))[0],
			total: (await itemService.getUserItems( user, 'owned' )).length
		}) ));
	}

	async register( req ) {
		const { name, email, password, isDeveloper } = req.body;
		const existing = await userService.getUserByEmail( email );
		if( existing ) throw new APIError('User already exist with the same email', 34)
		const user = await userService.register( name, email, password, isDeveloper ? 'developer' : 'consumer' );
		walletService.create( user ).catch( error => {
			this.logger.error( `createWallet`, {
				response: {
					code: error.code,
					message: error.message,
					stack: error.stack

				},
				request: JSON.stringify( req.body )
			} )
		} );

		const code = await redisService.setActivation( user );

		sendgridService.sendActivation( email, code ).catch( error => {
			this.logger.error( `register sendActivation`, {
				response: {
					code: error.code,
					message: error.message,
					stack: error.stack
				},
				request: JSON.stringify( req.body )
			} )
		} );

		const token = await redisService.setSession( user._doc );

		return {
			...user._doc,
			token,
			password: undefined
		}
	}

	async username( req ) {
		const { body: { username, avatar }, user: { id } } = req;
		const updatedUsername = await userService.updateUsername( id, username );
		const updatedAvatar = await userService.updateAvatar( id, avatar );

		return updatedUsername && updatedAvatar;
	}

	async password( req ) {
		const { body: { password }, user: { id } } = req;
		return await userService.updatePassword( id, password );
	}

	async interest( req ) {
		const { body: { interest }, user: { id } } = req;
		return await userService.updateInterest( id, interest );
	}

	async currency( req ) {
		const { body: { currency }, user: { id } } = req;
		return await userService.updateCurrency( id, currency );
	}

	async role( req ) {
		const { body, params: { id } } = req;
		return await userService.updateRole( id, body.role );
	}

	async status( req ) {
		const { body, params: { id } } = req;
		return await userService.updateStatus( id, body.status );
	}

	async activate( req, res ) {
		const { code } = req.params;
		const userId = await redisService.getActivation( code );
		if( !userId ) return res.redirect( `${config.server.front}/#/login?result=token_expired` );

		const user = await userService.getUserById( userId );
		if( !user ) return res.redirect( `${config.server.front}/#/login?result=user_not_found` );

		const activated = await userService.activate( userId );
		if( !activated ) return res.redirect( `${config.server.front}/#/login?result=error_activating` );

		res.redirect( `${config.server.front}/#/login?result=success` );
	}

	async topcreators(){
		// const creators = await transactionService.getTopCreators();
		const creators = await userService.getTopCreators();
		const users = [];
		for await ( const creator of creators ){
			users.push( await userService.getUserById(creator._id) );
		}
		return users;
	}

	async topusers(){
		const creators = await userService.getTopCreators();
		const users = [];
		for await ( const creator of creators ){
			users.push( {
				...(await userService.getUserById(creator._id))._doc,
				onCollection: creator.count,
			} );
		}
		return users;
	}

	async developer( req ){
		const { email } = req.body;
		const user = await userService.getUserByEmail( email );
		sendgridService.sendDeveloper( email, user.username ).catch( error => {
			this.logger.error( `register sendDeveloper`, {
				response: {
					code: error.code,
					message: error.message,
					stack: error.stack
				},
				request: JSON.stringify( req.body )
			} )
		} );
	}
}