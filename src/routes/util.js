import Router from "./router.js";
import axios from "axios";
import { getPrice } from "../util/ethereum.js";

export default class UtilRouter extends Router {

  constructor( logger ) {
    super( logger );
  }

  init() {
    this.cached( '/price/eth', ['PUBLIC'], '1 minute', this.priceETH );
  }

  async priceETH() {
    return getPrice();
  }

}