import Router from "./router.js";
import { statsService } from "../services/index.js";

export default class StatsRouter extends Router {

	constructor( logger ) {
		super( logger );
	}

	init() {
		this.get( '/user', ['AUTHENTICATED'], this.currentUserStats );
		this.get( '/user/:id', ['AUTHENTICATED'], this.userStats );
		this.get( '/items', ['AUTHENTICATED'], this.itemsStats );
		this.get( '/users', ['AUTHENTICATED'], this.usersStats );
	}

	async itemsStats(){
		const itemStats = await statsService.getItemStats();
		const txStats = await statsService.getTransactionStats();

		return { ...itemStats[0], ...txStats[0] };
	}

	async usersStats(){
		const stats = await statsService.getUsersStats();

		return { ...stats[0]};
	}

	async currentUserStats( req ){
		const { user } = req;
		const itemStats = await statsService.getUserItemStats( user.id );
		const txStats = await statsService.getUserTransactionStats( user.id );

		return { ...itemStats[0], ...txStats[0] };
	}

	async userStats( req ){
		const { id } = req.params;
		const itemStats = await statsService.getUserItemStats( id );

		return { ...itemStats[0] };
	}

}