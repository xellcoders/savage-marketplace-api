import Router from "./router.js";
import multer from "multer";
import {
	collectionService,
	itemService,
	nftService,
	transactionService,
	userService,
	walletService
} from "../services/index.js";
import APIError from "../util/error.js";
import { isAudio, isImage, isModel, isVideo } from "../util/image.js";
import config from "../config.js";

export default class APIRouter extends Router {

	constructor( logger ) {
		super( logger );
	}

	init() {
		this.post( '/nft/create', ['AUTHENTICATED'], this.create );
		this.get( '/nft/user/:id', ['AUTHENTICATED'], this.owned );
		this.put( '/nft/transfer', ['AUTHENTICATED'], this.transfer );
	}

	async create( req ) {
		const { body: data, user: { id } } = req;
		const user = await userService.getUserById( id );
		const wallets = await walletService.getUserWallets( user );
		if( !data.file) throw new APIError( "Debe subir primero el archivo", 60 );
		if( wallets.length <= 0 ) throw new APIError( "El usuario no tiene ninguna wallet creada", 50 );
		const wallet = wallets[0];
		const collection = await collectionService.getByName( data.collection, user ) || await collectionService.create( data.collection, user );
		if( isAudio( file.type ) ) {
			data.image = data.image || 'uri:audio:default';
		} else if( isImage( file.type ) ) {
			data.image = data.image || 'uri:image:' + config.storage.baseUri
		} else if( isVideo( file.type ) ) {
			data.image = data.image || 'uri:video:default'
		} else if( isModel( file.type ) ) {
			data.image = data.image || 'uri:model:default'
		}
		const nft = await nftService.create( data, user, collection, data.file.name );
		const items = [];
		for( const index of (new Array( nft.supply )).fill(0) ){
			const item = await itemService.create( nft, index, user, wallet );
			items.push( item );
		}
		return {
			address: nft.address,
			network: nft.chain,
			items: items.map( item => item._id )
		};
	}

	async owned( req ) {
		const { params: { id } } = req;
		const user = await userService.getUserById( id );
		const items = await itemService.getUserItems( user, 'owned' );
		return items.map( item => ({
			"id": item._id,
			"name": item.nft.name,
			"symbol": item.nft.symbol,
			"address": item.nft.address,
			"tokenId": item.tokenId,
			"image": item.nft.image,
			"hash": item.hash,
			"collection": item.nft.collection.name,
			"link": item.nft.link,
			"description": item.nft.description,
			"tags": item.nft.tags,
			"rarity": item.nft.rarity,
			"rating": item.rating,
			"unlockable": item.nft.unlockable,
			"explicit": item.nft.explicit,
			"supply": item.nft.supply,
			"chain": item.nft.chain,
			"royalties": item.nft.royalties,
			"instructions": item.instructions
		}) );
	}

	async transfer( req ) {
		const { params, user, body: { id, address, concept } } = req;
		const item = await itemService.getByUserAndId( user.id, id );
		if( item.status !== 'owned' ) throw new APIError('El NFT ya esta en venta, subasta o juego', 20);
		const wallet = await walletService.getWalletByAddress( address );
		if( !wallet ) throw new APIError('El usuario receptor no existe', 40, 404);
		await transactionService.transfer( item, user.id, wallet.user, concept );
		await itemService.transfer( params.id, user.id, wallet.user, concept );
		return true;
	}

}