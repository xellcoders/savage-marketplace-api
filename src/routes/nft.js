import Router from "./router.js";
import { collectionService, itemService, nftService, userService, walletService } from "../services/index.js";
import APIError from "../util/error.js";
import multerGoogleStorage from "multer-google-storage";
import config from "../config.js";
import * as cloud from "@google-cloud/storage";
import { isAudio, isImage, isModel, isVideo } from "../util/image.js";

export default class NFTRouter extends Router {

	constructor( logger ) {
		super( logger );
	}

	init() {
		const storage = multerGoogleStorage.storageEngine({
			autoRetry: true,
			bucket: config.storage.bucket,
			projectId: 'xoy-coin',
			keyFilename: config.storage.keyFile,
			filename: (req, file, cb) => {
				cb(null, `${Date.now()}_${file.originalname}`);
			}
		});
		this.gcloud = new cloud.default.Storage({ keyFilename: config.storage.keyFile });

		this.multer( '/', ['AUTHENTICATED'], storage, this.create );
		this.get( '/', ['AUTHENTICATED'], storage, this.create );
		this.get( '/:id/resource', ['PUBLIC'], this.resource );
		this.get( '/collections', ['AUTHENTICATED'], this.collections );
	}

	async create( req ) {
		const { body, files, user: { id } } = req;
		const data = JSON.parse( body.data );
		const user = await userService.getUserById( id );
		const wallets = await walletService.getUserWallets( user );
		if( wallets.length <= 0 ) throw new APIError( "El usuario no tiene ninguna wallet registrada", 50 );
		const collection = await collectionService.getByName( data.collection, user ) || await collectionService.create( data.collection, user );
		if( isAudio( files[0].mimetype ) ) {
			data.image = data.image || 'uri:audio:default';
		} else if( isImage( files[0].mimetype ) ) {
			data.image = data.image || `uri:image:${config.storage.baseUri}`
		} else if( isVideo( files[0].mimetype ) ) {
			data.image = data.image || 'uri:video:default'
		} else if( isModel( files[0].mimetype ) ) {
			data.image = data.image || 'uri:model:default'
		}
		const nft = await nftService.create( data, user, collection, files[0].filename );
		const items = [];
		for( const index of (new Array( nft.supply )).fill(0) ){
			const item = await itemService.create( nft, index, user, wallets[0] );
			items.push( item );
		}
		return {
			address: nft.address,
			network: nft.chain,
			items: items.map( item => item._id )
		};
	}

	async collections( req ) {
		const { user: { id } } = req;
		const user = await userService.getUserById( id );
		return await collectionService.getByUser( user );
	}

	async resource( req, res ) {
		const { params: { id } } = req;
		const image = await nftService.getImage( this.gcloud, id );
		res.writeHead( 200, {
			'Content-Type': 'image/png',
			'Content-Disposition': `attachment;filename=image.png`,
		} );
		res.end( image[0] );
	}

}