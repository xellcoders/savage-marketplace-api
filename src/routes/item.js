import moment from "moment";
import Router from "./router.js";
import {
	commentService,
	itemService,
	transactionService,
	userService,
	walletService
} from "../services/index.js";
import APIError from "../util/error.js";
import filter from "../util/filter.js";
import { hasBalance, toEth } from "../util/ethereum.js";

export default class ItemRouter extends Router {

	constructor( logger ) {
		super( logger );
	}

	init() {
		this.get( '/', ['PUBLIC'], this.list );
		this.get( '/owned', ['AUTHENTICATED'], this.owned );
		this.get( '/onsale', ['PUBLIC'], this.onsale );
		this.get( '/onauction', ['PUBLIC'], this.onauction );
		this.get( '/onsale/user', ['AUTHENTICATED'], this.onsaleuser );
		this.get( '/onauction/user', ['AUTHENTICATED'], this.onauctionuser );
		this.get( '/ongame', ['AUTHENTICATED'], this.ongame );
		this.get( '/sold', ['AUTHENTICATED'], this.sold );
		this.get( '/bought', ['AUTHENTICATED'], this.bought );
		this.get( '/top', ['AUTHENTICATED'], this.top );
		this.get( '/preferred', ['AUTHENTICATED'], this.preferred );
		this.get( '/liked', ['AUTHENTICATED'], this.liked );
		this.get( '/user/:uid', ['AUTHENTICATED'], this.user );
		this.get( '/:id', ['PUBLIC'], this.view );
		this.get( '/:id/rating', ['AUTHENTICATED'], this.rating );
		this.put( '/:id/sell', ['AUTHENTICATED'], this.sell );
		this.put( '/:id/auction', ['AUTHENTICATED'], this.auction );
		this.put( '/:id/game', ['AUTHENTICATED'], this.game );
		this.put( '/:id/transfer', ['AUTHENTICATED'], this.transfer );
		this.put( '/:id/buy', ['AUTHENTICATED'], this.buy );
		this.put( '/:id/bid', ['AUTHENTICATED'], this.bid );
		this.put( '/:id/like', ['AUTHENTICATED'], this.like );
		this.put( '/:id/rate', ['AUTHENTICATED'], this.rate );
		this.put( '/:id/comment', ['AUTHENTICATED'], this.comment );
		this.get( '/:id/comments', ['AUTHENTICATED'], this.comments );
	}

	async list( req ) {
		const { user, query: { filters } } = req;
		const items = await itemService.getActiveItems();
		if( !user ) return items;
		const all = await Promise.all( items.map( async item => {
			const liked = await itemService.getLike( item._id, user.id );
			return {
				liked,
				...item._doc
			}
		} ) );
		const filtered = filter( all, filters );
		return filtered.filter( item => moment().isBefore(moment(item.deadline)) || item.status !== 'onauction' )
	}

	async view( req ){
		const { params:{ id }, user } = req;
		const item = await itemService.getById( id );
		const available = await itemService.getAvailable( item.nft._id );
		if( !user ) {
			return {
				...item._doc,
				available: available.length
			};
		}
		const like = await itemService.getLike( id, user.id );
		return {
			liked: !!like,
			...item._doc,
			available: available.length
		};
	}

	async owned( req ){
		const { user: { id } } = req;
		const user = await userService.getUserById( id );
		const items = await itemService.getUserItemsOwned( user );
		return items.map( item => item._doc );
	}

	async user( req ){
		const { uid } = req.params;
		const user = await userService.getUserById( uid );
		const items = await itemService.getUserItemsOwned( user );
		return items.map( item => item._doc );
	}

	async onsale( req ){
		const userId = req?.user?.id;
		const { query: { filters } } = req;
		const items = await itemService.getItemsByStatus( 'onsale' );
		const all = await Promise.all( items.map( async item => {
			const liked = userId ? await itemService.getLike( item._id, userId ) : false;
			return {
				liked,
				...item._doc
			}
		} ) );
		return filter( all, filters );
	}

	async onauction( req ){
		const userId = req?.user?.id;
		const { query: { filters } } = req;
		const items = await itemService.getItemsByStatus( 'onauction' );
		const all = await Promise.all( items.map( async item => {
			const liked = userId ? await itemService.getLike( item._id, userId ) : false;
			return {
				liked,
				...item._doc
			}
		} ) );
		const filtered = filter( all, filters );
		return filtered.filter( item => moment().isBefore(moment(item.deadline)) )
	}

	async onsaleuser( req ){
		const { user: { id }, query: { filters } } = req;
		const user = await userService.getUserById( id );
		const items = await itemService.getUserItems( user, 'onsale' );
		return filter( items, filters );
	}

	async onauctionuser( req ){
		const { user: { id }, query: { filters } } = req;
		const user = await userService.getUserById( id );
		const items = await itemService.getUserItems( user, 'onauction' );
		const filtered = filter( items, filters );
		return filtered.filter( item => moment().isBefore(moment(item.deadline)) )
	}

	async ongame( req ){
		const { user: { id } } = req;
		const user = await userService.getUserById( id );
		const items = await itemService.getUserItems( user, 'ongame' );
		return items.map( item => item._doc );
	}

	async sold( req ){
		const { user: { id } } = req;
		const user = await userService.getUserById( id );
		const items = await transactionService.getSoldItems( user );
		return items.map( item => item._doc );
	}

	async top( req ){
		const { user } = req;
		const sold = await transactionService.getTopItems();
		const items = await itemService.getItemsByIds( sold.map( item => item._id ) );
		return await Promise.all( items.map( async item => {
			const liked = await itemService.getLike( item._id, user.id );
			return {
				liked: !!liked,
				...item._doc
			}
		} ) );
	}

	async bought( req ){
		const { user: { id } } = req;
		const user = await userService.getUserById( id );
		const items = await transactionService.getBoughtItems( user );
		return items.map( item => item._doc );
	}

	async preferred( req ){
		const { user: { id }, query: { filters } } = req;
		const user = await userService.getUserById( id );
		const items1 = await itemService.getItemsByTag( user, user.interest );
		const items2 = await itemService.getItemsByTag( user, `#${user.interest}` );
		const all = await Promise.all( items1.concat(items2).map( async item => {
			const liked = await itemService.getLike( item._id, user.id );
			return {
				liked,
				...item
			}
		} ) );
		const filtered = filter( all, filters );
		return filtered.filter( item => moment().isBefore(moment(item.deadline)) || item.status !== 'onauction' )
	}

	async liked( req ){
		const { user: { id }, query: { filters } } = req;
		const user = await userService.getUserById( id );
		const items = await itemService.getLikedByUser( user );
		const filtered = filter( items, filters );
		return filtered.filter( item => moment().isBefore(moment(item.deadline)) || item.status !== 'onauction' )
	}

	async sell( req ){
		const { params, user, body: { price, currency } } = req;
		const item = await itemService.getByUserAndId( user.id, params.id );
		if( item.status !== 'owned' ){
			throw new APIError('El NFT ya esta en venta ó subasta', 20);
		}
		return await itemService.putOnSale( params.id, user.id, price, currency );
	}

	async auction( req ){
		const { params, user, body: { start, days, hours, minutes, currency } } = req;
		const item = await itemService.getByUserAndId( user.id, params.id );
		if( item.status !== 'owned' ){
			throw new APIError('El NFT ya esta en venta, subasta o juego', 20);
		}
		const deadline = moment().add(days, 'days').add(hours, 'hours').add(minutes , 'minutes');
		await transactionService.putAuction( item, user.id, start, deadline );
		return await itemService.putOnAuction( params.id, user.id, start, deadline, currency );
	}

	async game( req ){
		const { params, user, body: { instructions } } = req;
		const item = await itemService.getByUserAndId( user.id, params.id );
		if( item.status !== 'owned' ){
			throw new APIError('El NFT ya esta en venta, subasta o juego', 20);
		}
		return await itemService.putOnGame( params.id, user.id, instructions );
	}

	async bid( req ){
		const { params, user, body  } = req;
		const item = await itemService.getById( params.id );
		if( item.status !== 'onauction' ) throw new APIError('El NFT no esta en subasta', 25);
		const { address } = req.body;
		const balance = await walletService.getEthereumBalance(address, 'ethereum');
		const price = await toEth( item.price, item.currency )
		if( balance < price ) throw new APIError('No tiene fondos suficientes', 25);
		if( body.bid < item.start ) throw new APIError('El precio ofertado es menor al precio inicial', 29);
		if( (item.bid || 0) <= body.bid ) await itemService.bid( params.id, item.owner, body.bid );
		return await transactionService.bid( item, user.id, body.bid );
	}

	async buy( req ){
		const { params, user } = req;
		const item = await itemService.getById( params.id );
		// const wallet = await walletService.getUserWallets( user.id );
		const { address } = req.body;
		const balance = await walletService.getEthereumBalance(address, 'ethereum');
		const price = await toEth( item.price, item.currency )
		if( balance < price ) throw new APIError('No tiene fondos suficientes', 25);

		if( item.status !== 'onsale' ){
			throw new APIError('El NFT no esta en venta', 20);
		}
		await transactionService.sell( item, item.owner, item.price );
		await transactionService.buy( item, user.id, item.price );
		return await itemService.changeOwner( params.id, user.id );
	}

	async transfer( req ){
		const { params, user, body: { address, concept } } = req;
		const item = await itemService.getByUserAndId( user.id, params.id );
		if( item.status !== 'owned' ) throw new APIError('El NFT ya esta en venta, subasta o juego', 20);
		const wallet = await walletService.getWalletByAddress( address );
		if( !wallet ) throw new APIError('El usuario receptor no existe', 40, 404);
		await transactionService.transfer( item, user.id, wallet.user, concept );
		return await itemService.transfer( params.id, user.id, wallet.user, concept );
	}

	async like( req ){
		const { params, user } = req;
		const like = await itemService.getLike( params.id, user.id );

		if( like ) {
			await itemService.unlike( params.id, user.id );
			return false;
		}
		await itemService.like( params.id, user.id );
		return true;
	}

	async comment( req ){
		const { params, user, body } = req;
		return await commentService.addComment( params.id, user.id, body.comment );
	}

	async comments( req ){
		const { params } = req;
		return await commentService.getItemComments( params.id );
	}

	async rate( req ){
		const { params, user, body } = req;
		return await itemService.rate( params.id, user.id, body.score );
	}

	async rating( req ){
		const { params, user } = req;
		return await itemService.getRating( params.id, user.id );
	}

}