import Wallet from "../model/wallet.js";
import { createEthereumKeyPair, getBalance } from "../util/ethereum.js";
import { getERC20Balance } from "../util/contracts.js";
import config from "../config.js";

export default class WalletService {

	async create( user, chain='Ethereum', manager='marketplace' ) {
		const keyPair = createEthereumKeyPair();
		const wallet = new Wallet( {
			user,
			chain,
			address: keyPair.address,
			privateKey: keyPair.privateKey,
			manager
		} );
		return wallet.save();
	}

	async getUserWallets( user ) {
		return Wallet.find({ user });
	}

	async getWalletByAddress( address ) {
		return Wallet.findOne({ "address" : { $regex : new RegExp(address, "i") } });
	}

	async getEthereumBalance( address, network ) {
		switch( network ) {
			case 'ethereum':
				return await getBalance( address, "goerli" );
			case 'savage':
				return await getBalance( address, "goerli" );
		}
		return 0;
	}

	async getTokenBalance( address, token, network ) {
		switch( network ) {
			case 'ethereum':
				return await getERC20Balance( address, token, "ethereum" );
			case 'savage':
				return await getERC20Balance( address, token, "ethereum" );
		}
	}
}