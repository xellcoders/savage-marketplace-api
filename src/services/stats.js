import mongoose from "mongoose";
import Item from "../model/item.js";
import User from "../model/user.js";
import Transaction from "../model/transaction.js";

export default class StatsService {

  async getItemStats() {
    return Item.aggregate( [{
      $group: {
        _id: {},
        onSale: {
          $sum: { $cond: { if: { $eq: ["$status", 'onsale'] }, then: 1, else: 0 } }
        },
        onAuction: {
          $sum: { $cond: { if: { $eq: ["$status", 'onauction'] }, then: 1, else: 0 } }
        },
        onGame: {
          $sum: { $cond: { if: { $eq: ["$status", 'ongame'] }, then: 1, else: 0 } }
        },
        onCollection: {
          $sum: { $cond: { if: { $eq: ["$status", 'owned'] }, then: 1, else: 0 } }
        },
        inactive: {
          $sum: { $cond: { if: { $and: [{ $eq: ["$status", 'owned'] }, { $eq: ["$found", false] }, { $eq: ["$price", null] }, { $eq: ["$start", null] } ] }, then: 1, else: 0 } }
        },
        hodl: {
          $sum: { $cond: { if: { $and: [{ $eq: ["$status", 'owned'] }, { $or: [ { ne: ["$found", false] }, { $ne: ["$price", null] }, { $ne: ["$start", null] } ] } ] }, then: 1, else: 0 } }
        },
        found: {
          $sum: { $cond: { if: { $eq: ["$found", true] }, then: 1, else: 0 } }
        },
        total: {
          $sum: 1
        }
      }
    }] )
  }

  async getTransactionStats() {
    return Transaction.aggregate( [{
      $group: {
        _id: {},
        sold: {
          $sum: { $cond: { if: { $eq: ["$operation", 'sell'] }, then: 1, else: 0 } }
        },
        bought: {
          $sum: { $cond: { if: { $eq: ["$operation", 'buy'] }, then: 1, else: 0 } }
        },
        auction: {
          $sum: { $cond: { if: { $eq: ["$operation", 'onauction'] }, then: 1, else: 0 } }
        }
      }
    }] )
  }

  async getUsersStats() {
    return User.aggregate( [{
      $match: {
        "role": { $in: ["consumer", "creator", "developer", "admin"] }
      }
    }, {
      $group: {
        _id: {},
        consumers: {
          $sum: { $cond: { if: { $eq: ["$role", 'consumer'] }, then: 1, else: 0 } }
        },
        creators: {
          $sum: { $cond: { if: { $eq: ["$role", 'creator'] }, then: 1, else: 0 } }
        },
        admins: {
          $sum: { $cond: { if: { $eq: ["$role", 'admin'] }, then: 1, else: 0 } }
        },
        developer: {
          $sum: { $cond: { if: { $eq: ["$role", 'developer'] }, then: 1, else: 0 } }
        },
        active: {
          $sum: { $cond: { if: { $eq: ["$status", 'active'] }, then: 1, else: 0 } }
        },
        inactive: {
          $sum: { $cond: { if: { $eq: ["$status", 'inactive'] }, then: 1, else: 0 } }
        },
        members: {
          $sum: 1
        }
      }
    }] )
  }

  async getUserItemStats( userId ) {
    return Item.aggregate( [{
      $match: {
        "owner": mongoose.Types.ObjectId( userId )
      }
    }, {
      $group: {
        _id: {},
        onSale: {
          $sum: { $cond: { if: { $eq: ["$status", 'onsale'] }, then: 1, else: 0 } }
        },
        onAuction: {
          $sum: { $cond: { if: { $and: [{ $eq: ["$status", 'onauction'] }, { $gte: ["$deadline", new Date()] } ] }, then: 1, else: 0 } }
        },
        onGame: {
          $sum: { $cond: { if: { $eq: ["$status", 'ongame'] }, then: 1, else: 0 } }
        },
        onCollection: {
          $sum: { $cond: { if: { $eq: ["$status", 'owned'] }, then: 1, else: 0 } }
        },
        total: {
          $sum: 1
        }
      }
    }] )
  }

  async getUserTransactionStats( userId ) {
    return Transaction.aggregate( [{
      $match: {
        "user": mongoose.Types.ObjectId( userId )
      }
    }, {
      $group: {
        _id: {},
        sold: {
          $sum: { $cond: { if: { $eq: ["$operation", 'sell'] }, then: 1, else: 0 } }
        },
        bought: {
          $sum: { $cond: { if: { $eq: ["$operation", 'buy'] }, then: 1, else: 0 } }
        }
      }
    }] )
  }

}