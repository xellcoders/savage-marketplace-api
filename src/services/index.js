import redis from "redis";
import mongoose from "mongoose";
import AuthService from "./auth.js";
import UserService from "./user.js";
import SendgridService from "./sendgrid.js";
import RedisService from "./redis.js";
import WalletService from "./wallet.js";
import NFTService from "./nft.js";
import ItemService from "./item.js";
import CollectionService from "./collection.js";
import StatsService from "./stats.js";
import TransactionService from "./transaction.js";
import CommentService from "./comment.js";
import config from "../config.js";

const redisClient = redis.createClient( config.redis );

export const initServices = async() => {
	console.log( 'Connecting to redis', config.redis.url );
	await redisClient.connect();

	console.log( 'Connecting to mongo', config.mongo.url );
	await mongoose.connect( config.mongo.url );
}

export const redisService = new RedisService( redisClient );
export const authService = new AuthService();
export const userService = new UserService();
export const walletService = new WalletService();
export const nftService = new NFTService();
export const itemService = new ItemService();
export const collectionService = new CollectionService();
export const statsService = new StatsService();
export const transactionService = new TransactionService();
export const commentService = new CommentService();
export const sendgridService = new SendgridService();