import sendGrid from "@sendgrid/mail"
import config from "../config.js";

export default class SendgridService {

	constructor() {
		sendGrid.setApiKey( config.sendgrid.apikey );
	}

	async sendActivation( to, code ) {
		return await sendGrid.send( {
			to,
			from: config.sendgrid.sender,
			templateId: config.sendgrid.activationTemplate,
			dynamicTemplateData: { activationUrl: `${config.server.domain}/user/activate/${code}` }
		} )
	}

	async sendDeveloper( email, user ) {
		return await sendGrid.send( {
			to: 'neonsavage.marketplace@gmail.com',
			from: config.sendgrid.sender,
			templateId: config.sendgrid.developerTemplate,
			dynamicTemplateData: { user, email }
		} )
	}
}