import randomstring from "randomstring";

export default class RedisService {

	constructor( redisClient ) {
		this.redisClient = redisClient;
	}

	async setSession( user ) {
		const token = randomstring.generate();
		await this.redisClient.setEx( token, 60 * 60, `${user._id.toString()}|${user.role}` );
		return token;
	}

	async getSession( token ) {
		const session = await this.redisClient.get( token );
		if( !session ) return null;
		const [id, role] = session.split('|');
		return { id, role };
	}

	async setActivation( user ) {
		const code = randomstring.generate();
		await this.redisClient.setEx( code, 60 * 60, user._id.toString() );
		return code;
	}

	async getActivation( code ) {
		const activation = await this.redisClient.get( code );
		await this.redisClient.del( code );
		return activation;
	}
}
