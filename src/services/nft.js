import NFT from "../model/nft.js";
import { deployNFT } from "../util/contracts.js";
import config from "../config.js";

export default class NFTService {

  async create( data, user, collection, filename ) {
    const nft = new NFT( {
      name: data.name,
      symbol: data.symbol,
      category: collection,
      image: data.image,
      link: data.link,
      description: data.description,
      tags: data.tags?.map( tag => tag.toLowerCase().trim() ),
      rarity: data.rarity,
      supply: data.supply,
      unlockable: data.unlockable,
      explicit: data.explicit,
      royalties: data.royalties,
      creator: user,
      chain: data.chain,
      uri: data.uri || config.storage.baseUri,
      filename
    } );
    const saved = await nft.save();

    const uri = data.uri || saved.uri + saved._id

    const { address, hash } = await deployNFT( data.name, data.symbol, uri, data.chain );
    saved.address = address;
    saved.hash = hash;
    saved.status = 'deployed';
    saved.uri = uri;

    return saved.save();
  }

  async getImage( storage, id ) {
    const nft = await NFT.findOne( { _id: id } );
    const bucket = await storage.bucket( config.storage.bucket );
    return await bucket.file( nft.filename ).download();
  }

}