import Collection from "../model/collection.js";

export default class CollectionService {

	async create( name, user ) {
		const collection = new Collection({ name, user });
		return collection.save();
	}

	async getByName( name, user ) {
		return Collection.findOne({ name, user });
	}

	async getByUser( user ) {
		return Collection.find({ user });
	}

}