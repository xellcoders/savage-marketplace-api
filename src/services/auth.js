import bcrypt from "bcrypt-nodejs";
import User from "../model/user.js";

export const USER_NOT_FOUND = 1;
export const INVALID_CREDENTIALS = 2;

export default class AuthService {

	async login( email, password ) {
		const user = await User.findOne( { email } );
		if( !user ) return USER_NOT_FOUND;
		try {
			if( !bcrypt.compareSync( password, user.password ) )
				return INVALID_CREDENTIALS;

			return user._doc;
		} catch( error ) {
			console.log( error );
			return INVALID_CREDENTIALS;
		}
	}

}
