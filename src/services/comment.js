import Comment from "../model/comment.js";

export default class CommentService {

  async getItemComments( item ){
    return Comment.find( { item } ).populate('user', 'name avatar').sort({ 'created_at': -1 });
  }

  async addComment( item, user, text ) {
    const comment = new Comment( { user, item, text } )
    return comment.save();
  }
}