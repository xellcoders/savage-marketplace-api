import bcrypt from "bcrypt-nodejs";
import User from "../model/user.js";

export default class UserService {

	async register( name, email, password, role='consumer' ) {
		const hash = bcrypt.hashSync( password, bcrypt.genSaltSync( 8 ), null );
		const user = new User( {
			name,
			email,
			role,
			password: hash
		} );
		return user.save();
	}

	async getUsers( username = '', role, status ) {
		return User.find({ username: new RegExp(`^${username}`), role: { $in: role }, status: { $in: status } })
	}

	async getTopCreators() {
		return await User.aggregate( [{
			$lookup: {
				from: "items",
				localField: "_id",
				foreignField: "owner",
				as: "items"
			}
		}, {
			$project: {
				_id: 1,
				count: { $size: "$items" }
			}
		}, { $sort: { count: -1 } }, { $limit: 10 }] );
	}

	async updateUsername( id, username ) {
		const result = await User.updateOne( { _id: id }, { username } );
		return result.modifiedCount === 1;
	}

	async updatePassword( id, password ) {
		const hash = bcrypt.hashSync( password, bcrypt.genSaltSync( 8 ), null );
		const result = await User.updateOne( { _id: id }, { password: hash } );
		return result.modifiedCount === 1;
	}

	async updateAvatar( id, avatar ) {
		const result = await User.updateOne( { _id: id }, { avatar } );
		return result.modifiedCount === 1;
	}

	async updateInterest( id, interest ) {
		const result = await User.updateOne( { _id: id }, { interest } );
		return result.modifiedCount === 1;
	}

	async updateCurrency( id, currency ) {
		const result = await User.updateOne( { _id: id}, { currency } );
		return result.modifiedCount === 1;
	}

	async updateRole( id, role ) {
		const result = await User.updateOne( { _id: id}, { role } );
		return result.modifiedCount === 1;
	}

	async updateStatus( id, status ) {
		const result = await User.updateOne( { _id: id}, { status } );
		return result.modifiedCount === 1;
	}

	async getUserById( id ){
		return User.findOne( { _id: id } );
	}

	async getUserByEmail( email ){
		return User.findOne( { email } );
	}

	async getUsersByIds( ids ){
		return User.find( { _id: { $in : ids } });
	}

	async activate( userId ) {
		const result = await User.updateOne( { _id: userId }, { verified: true } );
		return result.modifiedCount === 1;
	}

	async remove( id ) {
		const result = User.deleteOne( { _id: id } );
		return result.deletedCount === 1;
	}

}