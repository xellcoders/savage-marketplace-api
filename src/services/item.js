import Item from "../model/item.js";
import Like from "../model/like.js";
import Rate from "../model/rate.js";
import { mintItem } from "../util/contracts.js";

export default class ItemService {

  async create( nft, tokenId, user, wallet ) {
    const minted = await mintItem( nft.address, wallet.address, tokenId, `${nft.uri}/${tokenId}`, nft.chain );
    const item = new Item( { nft, tokenId, owner: user, beneficiary: user, wallet, hash: minted.hash } );
    return item.save();
  }

  async getById( id ) {
    return Item.findOne( { _id: id } )
        .populate( 'owner', '_id name username avatar' )
        .populate( 'beneficiary', '_id name username avatar' )
        .populate( 'wallet', '_id chain address' )
        .populate( {
          path: 'nft',
          populate: [{ path: 'category' }, { path: 'creator', select: '_id name username avatar' }]
        } );
  }

  async getAvailable( nft ) {
    return Item.find( { nft, status: { $in: ['onsale', 'onauction', 'ongame'] } } )
  }

  async getItemsByIds( ids ) {
    return Item.find( { _id: { $in: ids } } )
        .populate( 'owner', '_id name username avatar' )
        .populate( 'beneficiary', '_id name username avatar' )
        .populate( 'wallet', '_id chain address' )
        .populate( {
          path: 'nft',
          populate: [{ path: 'category' }, { path: 'creator', select: '_id name username avatar' }]
        } );
  }

  async getLikedByUser( user ) {
    return Item.aggregate( [{
      $lookup: {
        from: "likes",
        localField: "_id",
        foreignField: "item",
        as: "like"
      }
    }, {
      $match: {
        "like.user": user._id
      }
    }, {
      $lookup: {
        from: "nfts",
        localField: "nft",
        foreignField: "_id",
        as: "nft"
      }
    }, { $unwind: "$nft" }, {
      $lookup: {
        from: "users",
        localField: "owner",
        foreignField: "_id",
        as: "owner"
      }
    }, { $unwind: "$owner" }, {
      $lookup: {
        from: "users",
        localField: "beneficiary",
        foreignField: "_id",
        as: "beneficiary"
      }
    }, { $unwind: "$beneficiary" }, {
      $project: {
        _id: 1,
        price: 1,
        bid: 1,
        start: 1,
        status: 1,
        currency: 1,
        deadline: 1,
        instructions: 1,
        "owner._id": 1,
        "owner.name": 1,
        "owner.username": 1,
        "owner.avatar": 1,
        "beneficiary._id": 1,
        "beneficiary.name": 1,
        "beneficiary.username": 1,
        "beneficiary.avatar": 1,
        "nft._id": 1,
        "nft.name": 1,
        "nft.image": 1,
        "nft.category": 1,
        "nft.tags": 1,
        "nft.rarity": 1,
        "liked": "$like._id"
      }
    }] )
  }

  async getItemsByTag( user, tag ) {
    return Item.aggregate( [{
      $match: {
        status: { $in: ['onsale', 'onauction', 'ongame'] }
      }
    },
      {
        $lookup:
            {
              from: "nfts",
              localField: "nft",
              foreignField: "_id",
              as: "nft"
            }
      }, {
        $match: {
          "nft.tags": tag
        }
      },
      { $unwind: "$nft" },
      {
        $lookup:
            {
              from: "users",
              localField: "owner",
              foreignField: "_id",
              as: "owner"
            }
      },
      { $unwind: "$owner" },
      {
        $lookup:
            {
              from: "users",
              localField: "beneficiary",
              foreignField: "_id",
              as: "beneficiary"
            }
      },
      { $unwind: "$beneficiary" },
      {
        $project: {
          _id: 1,
          price: 1,
          currency: 1,
          bid: 1,
          start: 1,
          status: 1,
          deadline: 1,
          instructions: 1,
          "owner._id": 1,
          "owner.name": 1,
          "owner.username": 1,
          "owner.avatar": 1,
          "beneficiary._id": 1,
          "beneficiary.name": 1,
          "beneficiary.username": 1,
          "beneficiary.avatar": 1,
          "nft._id": 1,
          "nft.name": 1,
          "nft.image": 1,
          "nft.category": 1,
          "nft.tags": 1,
          "nft.rarity": 1
        }
      },
    ] )
  }

  async getByUserAndId( user, id ) {
    return Item.findOne( { owner: user, _id: id } )
        .populate( 'owner', '_id name username avatar' )
        .populate( 'beneficiary', '_id name username avatar' )
        .populate( 'wallet', '_id chain address' )
        .populate( {
          path: 'nft',
          populate: [{ path: 'category' }, { path: 'creator', select: '_id name username avatar' }]
        } );
  }

  async getUserItemsOwned( user ) {
    return Item.find( { owner: user } )
        .populate( 'owner', '_id name username avatar' )
        .populate( 'beneficiary', '_id name username avatar' )
        .populate( 'wallet', '_id chain address' )
        .populate( {
          path: 'nft',
          populate: [{ path: 'category', select: 'name' }, { path: 'rarity' }, { path: 'creator', select: '_id name username avatar' }]
        } );
  }

  async getUserItems( user, status ) {
    return Item.find( { owner: user, status } )
        .populate( 'owner', '_id name username avatar' )
        .populate( 'beneficiary', '_id name username avatar' )
        .populate( 'wallet', '_id chain address' )
        .populate( {
          path: 'nft',
          populate: [{ path: 'category', select: 'name' }, { path: 'rarity' }, { path: 'creator', select: '_id name username avatar' }]
        } );
  }

  async getItemsByStatus( status ) {
    return Item.find( { status } )
        .populate( 'owner', '_id name username avatar' )
        .populate( 'beneficiary', '_id name username avatar' )
        .populate( 'wallet', '_id chain address' )
        .populate( {
          path: 'nft',
          populate: [{ path: 'category', select: 'name' }, { path: 'rarity' }, { path: 'creator', select: '_id name username avatar' }]
        } );
  }

  async getActiveItems() {
    return Item.find( { status: { $in: ['onsale', 'onauction', 'ongame'] } } )
        .populate( 'owner', '_id name username avatar' )
        .populate( 'beneficiary', '_id name username avatar' )
        .populate( 'wallet', '_id chain address' )
        .populate( {
          path: 'nft',
          populate: [{ path: 'category', select: 'name' }, { path: 'rarity' }, { path: 'creator', select: '_id name username avatar' }]
        } ).sort({ created_at: 'desc' });
  }

  async putOnSale( id, owner, price, currency ) {
    const result = await Item.updateOne( { _id: id, owner }, { status: 'onsale', price, currency } );
    return result.modifiedCount === 1;
  }

  async putOnAuction( id, owner, start, deadline, currency ) {
    const result = await Item.updateOne( { _id: id, owner }, { status: 'onauction', start, deadline, currency } );
    return result.modifiedCount === 1;
  }

  async putOnGame( id, owner, instructions ) {
    const result = await Item.updateOne( { _id: id, owner }, { status: 'ongame', instructions } );
    return result.modifiedCount === 1;
  }

  async bid( id, owner, price ) {
    const result = await Item.updateOne( { _id: id, owner }, { bid: price } );
    return result.modifiedCount === 1;
  }

  async transfer( id, owner, receiver, concept ) {
    const result = await Item.updateOne( { _id: id }, { status: 'owned', owner: receiver, concept } );
    return result.modifiedCount === 1;
  }

  async changeOwner( id, owner ) {
    const result = await Item.updateOne( { _id: id }, { status: 'owned', owner } );
    return result.modifiedCount === 1;
  }

  async getLike( id, user ) {
    return Like.findOne( { item: id, user } );
  }

  async like( id, user ) {
    const like = new Like( { item: id, user } );
    return like.save();
  }

  async unlike( id, user ) {
    const result = await Like.deleteOne( { item: id, user } );
    return result.deletedCount === 1;
  }

  async getRating( id, user ) {
    const rating = await Rate.findOne( { item: id, user } );
    return rating ? rating.score : 0;
  }

  async rate( id, user, score ) {
    const item = await Item.findOne( { _id: id } );
    const current = await Rate.findOne( { item, user } );
    if( current ) {
      const value = item.rating - current.score <= 0 ? score : ( ( ( ( item.rating - current.score ) / 2.0 ) + current.score ) / 2.0 );
      await Item.updateOne( { _id: id }, { rating: value } );
      current.score = score;
      return await current.save();
    }
    const value = item.rating <= 0 ? score : ( item.rating + score ) / 2.0
    await Item.updateOne( { _id: id }, { rating: value } );
    const rating = await (new Rate( { item, user, score } )).save();
    return await rating.save();
  }

}