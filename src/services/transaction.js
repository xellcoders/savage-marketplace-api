import Transaction from "../model/transaction.js";

export default class TransactionService {

  async getUserTransactions( user ) {
    return Transaction.find( { user }).sort( { created_at: 'desc' } ).populate( {
      path: 'item',
      populate: [{ path: 'created_at' }, { path: 'price' }, { path: 'currency' }, { path: 'nft', select: 'name' }]
    } );
  }

  async getBoughtItems( user ) {
    const transactions = await Transaction.find( { user, operation: 'buy' } ).populate( {
      path: 'item',
      populate: [{ path: '_id' }, { path: 'created_at' }, { path: 'price' }, { path: 'currency' }, { path: 'nft', select: 'name image' }]
    } );
    return transactions.map( tx => tx.item );
  }

  async getSoldItems( user ) {
    const transactions = await Transaction.find( { user, operation: 'sell' } ).populate( {
      path: 'item',
      populate: [{ path: '_id' }, { path: 'created_at' }, { path: 'price' }, { path: 'currency' }, { path: 'nft', select: 'name image' }]
    } );
    return transactions.map( tx => tx.item );
  }

  async getTopItems() {
    const transactions = await Transaction.aggregate( [{
      $lookup: {
        from: "items",
        localField: "item",
        foreignField: "_id",
        as: "items"
      }
    }, {
      $match: {
        "items.status": { $in: [ 'onsale', 'onauction', 'ongame' ] }
      }
    }, {
      $group: {
        _id: "$item",
        count: {
          $sum: 1
        }
      }
    }, { $sort: { count: 1 } }, { $limit: 10 }] );
    return transactions.map( tx => tx._id );
  }

  async getTopCreators() {
    const transactions = await Transaction.aggregate( [{
      $group: {
        _id: "$user",
        count: { $sum: { $cond: { if: { $eq: ["$operation", 'create'] }, then: 1, else: 0 } } }
      }
    }, { $sort: { count: 1 } }, { $limit: 10 }] );
    return transactions.map( tx => tx._id );
  }

  async getTopUsers() {
    const transactions = await Transaction.aggregate( [{
      $group: {
        _id: "$user",
        count: {
          $sum: 1
        }
      }
    }, { $sort: { count: 1 } }, { $limit: 10 }] );
    return transactions.map( tx => tx._id );
  }

  async buy( item, user, price ) {
    const movement = new Transaction( { item, user, price, operation: 'buy' } );
    return movement.save();
  }

  async sell( item, user, price ) {
    const movement = new Transaction( { item, user, price, operation: 'sell' } );
    return movement.save();
  }

  async transfer( item, user, receiver, concept ) {
    const send = new Transaction( { item, user, receiver, concept, operation: 'transfer' } );
    const receive = new Transaction( { item, user: receiver, receiver, concept, operation: 'receive' } );
    return Promise.all( [send.save(), receive.save()] );
  }

  async bid( item, user, price ) {
    const movement = new Transaction( { item, user, bid: price, operation: 'bid' } );
    return movement.save();
  }

  async putAuction( item, user, start, deadline ) {
    const movement = new Transaction( { item, user, start, deadline, operation: 'onauction' } );
    return movement.save();
  }

}