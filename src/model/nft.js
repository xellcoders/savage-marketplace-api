import mongoose from "mongoose";

export default mongoose.model( 'nft',
		mongoose.Schema( {
			name: {
				type: String,
				required: true
			},
      symbol: {
        type: String,
        required: true
      },
      category: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'collection',
        required: true
      },
      ancestry: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'nft'
      },
      uri: {
        type: String,
        required: true
      },
      filename: String,
      image: String,
      media: {
        type: String,
        enum: ['image', 'audio', 'video', 'model'],
        default: 'image'
      },
      link: String,
      description: String,
      tags: [String],
      rarity: {
        type: String,
        enum: ['normal', 'rare', 'very', 'ultra', 'common', 'very-common', 'epic', 'legendary', 'mythical'],
        default: 'normal'
      },
      supply: {
        type: Number,
        required: true
      },
      unlockable: {
        type: Boolean,
        default: false
      },
      explicit: {
        type: Boolean,
        default: false
      },
      withdrawable: {
        type: Boolean,
        default: false
      },
      royalties: {
        type: Number,
        default: 0
      },
      features: String, // pending to include in figma
      skill: String, // pending to include in figma
      creator: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user',
        required: true
      },
      chain: {
        type: String,
        enum: ['savage', 'ethereum'],
        default: 'savage',
        required: true
      },
      address: {
        type: String
      },
      hash: {
        type: String
      },
      status: {
        type: String,
        enum: ['created', 'deployed', 'burned'],
        default: 'created'
      }
		}, {
			timestamps: {
				createdAt: 'created_at',
				updatedAt: 'updated_at'
			}
		} )
)