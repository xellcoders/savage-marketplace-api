import mongoose from "mongoose";

export default mongoose.model( 'like',
	mongoose.Schema( {
		item: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'item',
			required: true
		},
		user: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'user',
			required: true
		}
	}, {
		timestamps: {
			createdAt: 'created_at',
			updatedAt: 'updated_at'
		}
	} )
)