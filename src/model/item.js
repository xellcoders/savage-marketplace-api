import mongoose from "mongoose";

export default mongoose.model( 'item',
	mongoose.Schema( {
		nft: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'nft'
		},
		tokenId: {
			type: String,
			required: true
		},
		status: {
			type: String,
			enum: ['owned', 'onsale', 'onauction', 'ongame'],
			default: 'owned'
		},
		found: {
			type: Boolean,
			default: false
		},
		instructions: String,
		concept: String,
		price: {
			type: Number,
			min: 0
		},
		start: {
			type: Number,
			min: 0
		},
		bid: Number,
		deadline: Date,
		minStep: {
			type: Number,
			min: 0.00000001
		},
		currency: {
			type: String,
			enum: ['MXN', 'NSC', 'ETH'],
			default: 'ETH'
		},
		owner: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'user',
			required: true
		},
		rating: {
			type: Number,
			default: 0
		},
		beneficiary: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'user',
			required: true
		},
		wallet: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'wallet',
			required: true
		},
		hash: String
	}, {
		timestamps: {
			createdAt: 'created_at',
			updatedAt: 'updated_at'
		}
	} )
)