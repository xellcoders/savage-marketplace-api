import mongoose from "mongoose";

export default mongoose.model( 'collection',
	mongoose.Schema( {
		name: {
			type: String,
			required: true
		},
		user: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'user',
			required: true
		}
	}, {
		timestamps: {
			createdAt: 'created_at',
			updatedAt: 'updated_at'
		}
	} )
)