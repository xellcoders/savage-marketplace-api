import mongoose from "mongoose";

export default mongoose.model( 'user',
	mongoose.Schema( {
		name: {
			type: String,
			default: ''
		},
		email: {
			type: String
		},
		username: {
			type: String
		},
		password: {
			type: String,
			required: true
		},
		role: {
			type: String,
			enum: ['consumer', 'creator', 'developer'],
			default: 'consumer'
		},
		verified: {
			type: Boolean,
			default: false
		},
		currency: {
			type: String,
			enum: ['mxn', 'usd'],
			default: 'mxn'
		},
		avatar: String,
		interest: String,
		status: {
			type: String,
			enum: ['active', 'blocked', 'inactive'],
			default: 'active'
		}
	}, {
		timestamps: {
			createdAt: 'created_at',
			updatedAt: 'updated_at'
		}
	} )
)