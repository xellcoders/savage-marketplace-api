import mongoose from "mongoose";

export default mongoose.model( 'transaction',
	mongoose.Schema( {
		item: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'item',
			required: true
		},
		user: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'user',
			required: true
		},
		operation: {
			type: String,
			enum: ['create', 'pause', 'onsale', 'onauction', 'ongame', 'transfer', 'receive', 'sell', 'buy', 'bid', 'win', 'get'],
			required: true
		},
		duration: Number,
		price: Number,
		bid: Number,
		achievement: String,
		receiver: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'user'
		},
		concept: String
	}, {
		timestamps: {
			createdAt: 'created_at',
			updatedAt: 'updated_at'
		}
	} )
)