import mongoose from "mongoose";

export default mongoose.model( 'wallet',
	mongoose.Schema( {
		user: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'user',
			required: true
		},
		chain: {
			type: String,
			enum: ['Savage', 'Ethereum'],
			default: 'Savage',
			required: true
		},
		address: {
			type: String,
			required: true
		},
		privateKey: {
			type: String,
			required: true
		},
		manager: {
			type: String,
			enum: ['marketplace', 'metamask', 'savage-wallet'],
			default: 'marketplace'
		},
		enabled: {
			type: Boolean,
			default: true
		}
	}, {
		timestamps: {
			createdAt: 'created_at',
			updatedAt: 'updated_at'
		}
	} )
)